<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_issues', function (Blueprint $table) {
            $table->id();
            $table->dateTime('posting_date_time');
            $table->string('channel', 50);
            $table->string('client_name', 50);
            $table->longText('issue_details', 500);
            $table->string('issue_submitted_by');
            $table->string('issue_related_image');
            $table->dateTime('support_response_time');
            $table->dateTime('resolution_activity_start_at');
            $table->longText('primary_investigation', 500);
            $table->longText('secondary_investigation', 500);
            $table->string('final_status');
            $table->string('resolution_activity');
            $table->dateTime('issue_closing_time');
            $table->string('communication_on_issue_closing');
            $table->string('involved_team');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_issues');
    }
}
